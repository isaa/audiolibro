package com.example.isaa.audiolibrosv2;

/**
 * Created by isaa on 7/08/17.
 */

/**
 * Clase con los datos que contiene cada libro
 */
public class Datolibro {
    public String name;
    public String autor;
    public int resourceImage;
    public String url;

    /**
     * @param name          nombre del libro
     * @param autor         autor del libro
     * @param resourceImage para la caratula
     * @param url           audio del libro
     */

    public Datolibro(String name, String autor, int resourceImage, String url) {
        this.name = name;
        this.autor = autor;
        this.resourceImage = resourceImage;
        this.url = url;
    }

}

package com.example.isaa.audiolibrosv2;

import android.app.Activity;

import android.content.Context;
import android.media.MediaPlayer;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;

import com.example.spectrum.audiolibrosv2.R;

import java.io.IOException;

/**
<<<<<<< HEAD
 * Created by isaa on 8/08/17.
=======
 * Created by spectrum on 8/08/17.
>>>>>>> 5b1e64a170e440d8bb613e54e492f278513ae9e9
 */

public class DetalleFragment extends Fragment implements View.OnTouchListener, MediaPlayer.OnPreparedListener, android.widget.MediaController.MediaPlayerControl {
    public static String ARG_POSITION = "position";
    Activity actividad;
    MediaPlayer mediaPlayer;
    MediaController mediaController;

    @Override
    public void onAttach(Context context) {//onAttach (Activity a ) depreciado
        super.onAttach(context);
        actividad = getActivity();
    }

    public void updateBookView(int position) {
        setUpBookInfo(position, getView());
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        mediaController.show();
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        Log.d("Audiolibros", "Entramos en onPrepared de mediaplayer");
        mediaPlayer.start();
        mediaController.setMediaPlayer(this);
        mediaController.setAnchorView(actividad.findViewById(R.id.main_fragment_detalle));
    }


    @Override
    public void onStop() {
        super.onStop();
        try {
            mediaPlayer.stop();
            mediaPlayer.release();
        } catch (Exception e) {
            Log.d("Audiolibros", "Error en mediaPlayer.stop()");
        }
    }


    @Override
    public void pause() {
        mediaPlayer.pause();
    }

    @Override
    public int getDuration() {
        return mediaPlayer.getDuration();
    }

    @Override
    public int getCurrentPosition() {
        return mediaPlayer.getCurrentPosition();
    }

    @Override
    public void seekTo(int i) {
        mediaPlayer.seekTo(i);
    }


    @Override
    public boolean isPlaying() {
        return mediaPlayer.isPlaying();
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public void start() {
        mediaPlayer.start();
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getAudioSessionId() {
        return 0;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View inflatedView = inflater.inflate(R.layout.fragment_detalle, container, false);
        Bundle args = getArguments();
        if (args != null) {
            int position = args.getInt(ARG_POSITION);
            setUpBookInfo(position, inflatedView);
        } else {
            setUpBookInfo(0, inflatedView);
        }

        return inflatedView;
    }


    private void setUpBookInfo(int position, View view) {
        Datolibro datolibro = SelectorAdapter.bookVector.elementAt(position);
        TextView textView = (TextView) view.findViewById(R.id.textView1);
        TextView audiolibrotextView = (TextView) view.findViewById(R.id.textView2);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView1);
        imageView.setImageResource(datolibro.resourceImage);
        textView.setText(datolibro.autor);
        audiolibrotextView.setText(datolibro.name);
        try {
            mediaPlayer.stop();
            mediaPlayer.release();
        } catch (Exception e) {
            view.setOnTouchListener(this);

            Uri video = Uri.parse(datolibro.url);
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setOnPreparedListener(this);
            try {
                mediaPlayer.setDataSource(actividad, video);
                mediaPlayer.prepareAsync();
            } catch (IOException ex) {
                Log.e("error", "error no se puede reproducir" + ex.getMessage());
            }
            mediaController = new MediaController(actividad);
        }

    }

}
